RSpec.describe 'add' do
    context 'when input string is empty' do
        it 'returns 0' do
        expect(add("")).to eq(0)
        end
    end

    context 'when input string contains a single number' do
        it 'returns the number itself' do
          expect(add("1")).to eq(1)
          expect(add("5")).to eq(5)
        end
    end

    context 'when input string contains comma-separated numbers' do
        it 'returns the sum of numbers' do
          expect(add("1,5")).to eq(6)
          expect(add("10,20,30")).to eq(60)
          expect(add("2,3,4,5")).to eq(14)
        end
    end

    context 'when input string contains numbers separated by both comma and newline' do
        it 'returns the sum of numbers' do
          expect(add("1\n2,3")).to eq(6)
          expect(add("10\n20,30\n40")).to eq(100)
          expect(add("2,3\n4\n5")).to eq(14)
        end
    end

    context 'when input string contains invalid pattern' do
        it 'raises an exception' do
          expect { add("1,\n") }.to raise_error(ArgumentError, "Invalid input format")
        end
    end

    context 'when input string contains negative numbers' do
        it 'raises an exception with the negative numbers' do
          expect { add("-1,2,-3") }.to raise_error(ArgumentError, "Negative numbers not allowed: -1, -3")
        end
    end
end  